

<?php
$tituloEnlaces=['Inicio','Productos', 'Contactanos'];
$direccionesEnlaces=['inicio.php','productos.php', 'contacto.php'];


?>

<ul class="nav nav-tabs navbar-inverse"> <!-- navbar-inverse -->
	
	<?php
	for($i=0;$i<count($tituloEnlaces);$i++){
		if($direccionesEnlaces[$i]==$p){
			$activo=' class="active"';
		}else{
			$activo='';
		}

	?>

	<li <?php echo $activo; ?>>
		<a href="index.php?p=<?php echo $direccionesEnlaces[$i]; ?>">
			<?php echo $tituloEnlaces[$i]; ?>
		</a>
	</li>

	<?php } ?>

</ul>